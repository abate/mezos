#!/bin/bash
set -x

CYCLE=50
SNAPS=(12)

for i in "${SNAPS[@]}"; do
    ./_build/default/src/mezos_delegate_pay.exe $CYCLE $i
    CYCLE=$((CYCLE+1))
    sleep 60s
done

