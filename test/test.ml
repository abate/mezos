open Lwt.Infix
open Mezos_db_common
open Mezos_lib_common

let check_db (module DB : Db.DB) _switch () =
  let db = Sqlexpr_sqlite_lwt.open_db ":memory:"
      ~init:begin fun db ->
        let inited = DB.auto_init_db db Format.err_formatter in
        let checked = DB.check_db db Format.err_formatter in
        if not (inited && checked) then
          failwith "db check error"
      end in
  Sqlexpr_sqlite_lwt.execute
    db Db.enable_foreign_keys_pragma >>= fun () ->
  Sqlexpr_sqlite_lwt.close_db db ;
  Lwt.return_unit

let db_init = Alcotest_lwt.[
    test_case "wallet_db" `Quick (check_db (module Wallet_db)) ;
    test_case "chain_db" `Quick (check_db (module Chain_db)) ;
]

let () =
  Alcotest.run "mezos" [
    "db_init", db_init ;
  ]
