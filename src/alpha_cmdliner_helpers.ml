(*---------------------------------------------------------------------------
   Copyright (c) 2018 Vincent Bernardoff. All rights reserved.
   Distributed under the ISC license, see terms at the end of the file.
  ---------------------------------------------------------------------------*)

open Rresult
open Cmdliner
open Cmdliner_helpers

module Convs = struct
  let tez =
    let open Alpha_context.Tez in
    let parser v =
      match of_string v with
      | None -> Error (`Msg "Invalid tez amount")
      | Some s -> Ok s in
    Arg.conv ~docv:"tez" (parser, pp)

  let address =
    let open Tezos_crypto.Signature.Public_key_hash in
    let parser v =
      match of_b58check v with
      | Error _e -> R.error_msgf "%s is not a valid pkh" v
      | Ok addr -> Ok addr in
    Arg.conv ~docv:"pkh" (parser, pp)

  let contract =
    let open Alpha_context.Contract in
    let parser v =
      match of_b58check v with
      | Error _e -> R.error_msgf "%s is not a valid contract id" v
      | Ok addr -> Ok addr in
    Arg.conv ~docv:"address" (parser, pp)

  let public_key =
    let open Tezos_base.TzPervasives in
    let parser v =
      R.error_to_msg ~pp_error:pp_print_error
        (Signature.Public_key.of_b58check v) in
    Arg.conv ~docv:"public key" (parser, Signature.Public_key.pp)
end

module Terms = struct
  let fee =
    let doc = "Transaction fee in mutez" in
    let open Alpha_context in
    Arg.(value & opt Convs.tez Tez.zero & info ["fee"] ~doc ~docv:"mutez")
end

(*---------------------------------------------------------------------------
   Copyright (c) 2018 Vincent Bernardoff

   Permission to use, copy, modify, and/or distribute this software for any
   purpose with or without fee is hereby granted, provided that the above
   copyright notice and this permission notice appear in all copies.

   THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
   WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
   MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
   ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
   WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
   ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
   OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
  ---------------------------------------------------------------------------*)
