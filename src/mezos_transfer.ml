(*---------------------------------------------------------------------------
   Copyright (c) 2018 Vincent Bernardoff. All rights reserved.
   Distributed under the ISC license, see terms at the end of the file.
  ---------------------------------------------------------------------------*)

open Alpha_context

let src = Logs.Src.create "mezos.transfer"

let default_fee_parameter =
  { Injection.minimal_fees = Obj.magic 100L ;
    minimal_nanotez_per_gas_unit = Z.of_int 100 ;
    minimal_nanotez_per_byte = Z.of_int 1000 ;
    force_low_fee = false ;
    fee_cap = Obj.magic 10_000_000L ;
    burn_cap = Obj.magic 10_000_000L ;
  }

let create_mgr_operation ~source ~counter ~operation =
  Manager_operation {
    source ;
    fee = Tez.zero ;
    counter ;
    operation ;
    gas_limit = Z.minus_one ;
    storage_limit = Z.minus_one }

let increase_mgr_op_counter = function
  | Manager_operation ({ counter ; _ } as mgrop) ->
    Manager_operation { mgrop with counter = Z.succ counter }

let pack_mgr_operation
    (type kind)
    ?reveal
    ~source ~counter
    ~(operation : kind Alpha_context.manager_operation) =
  let open Alpha_context in
  let mgr_operation = create_mgr_operation ~source ~counter ~operation in
  match reveal with
  | None ->
    return (Contents_list (Single mgr_operation), Z.succ counter)
  | Some pk -> begin
      let reveal_op =
        create_mgr_operation ~source ~counter ~operation:(Reveal pk) in
      let ops =
        Cons (reveal_op,
              Single (increase_mgr_op_counter mgr_operation)) in
      return (Contents_list ops, Z.(succ (succ counter)))
    end

type destination = {
  destination : Contract.t ;
  amount : Tez.t ;
  parameters : Script.lazy_expr option ;
}

let create_destination ?parameters ~destination ~amount () =
  { destination ; amount ; parameters }

let pp_destination ppf { destination ; amount } =
  Format.fprintf ppf "@[<0>{ @[<0>dst: %a@ amount: %a@] }@]"
    Contract.pp destination Tez.pp amount

let destination_encoding =
  let open Data_encoding in
  conv
    (fun { destination ; amount ; parameters} ->
       (destination, amount, parameters, None))
    (fun (destination, amount, parameters, _) ->
       { destination ; amount ; parameters })
    (obj4
       (req "dst" Contract.encoding)
       (req "amount" Tez.encoding)
       (opt "parameters" Script.lazy_expr_encoding)
       (opt "fee" Tez.encoding))

type 'a manager_operations = {
  src : Contract.t ;
  src_pk : Signature.public_key ;
  ops : 'a list ;
}

let pp_manager_operations pp_op  ppf { src ; src_pk ; ops } =
  Format.fprintf ppf "@[<0>{ @[<0>src: %a@ src_pk: %a@ ops: %a@] }@]"
    Contract.pp src
    Signature.Public_key.pp src_pk
    (Format.pp_print_list pp_op) ops

let create_transfer ~src ~src_pk ~dsts =
  { src ; src_pk ; ops = dsts }

let manager_operations_encoding encoding =
  let open Data_encoding in
  conv
    (fun { src ; src_pk ; ops } -> (src, src_pk, ops))
    (fun (src, src_pk, ops) -> { src ; src_pk ; ops })
    (obj3
       (req "src" Contract.encoding)
       (req "src_pk" Signature.Public_key.encoding)
       (req "dsts" (list encoding)))

let pack_tx ?reveal ~source ~counter { destination ; amount ; parameters } =
  let operation = Alpha_context.Transaction
      { amount ; parameters ; destination } in
  pack_mgr_operation ?reveal ~source ~counter ~operation

let pack_delegation ?reveal ~source ~counter maybe_pkh =
  let operation = Alpha_context.Delegation maybe_pkh in
  pack_mgr_operation ?reveal ~source ~counter ~operation

let patch_fees ~fee_parameter cctxt (Contents_list ops) =
  Injection.may_patch_limits cctxt
    ~fee_parameter ~chain:`Main ~block:(`Head 0)
    ~compute_fee:true ops >>=? fun ops ->
  Lwt.return (Injection.get_manager_operation_gas_and_fee ops) >>=? fun (fees, gas) ->
  return (Contents_list ops, fees, gas)

type forged_mgr_op = {
  payload : MBytes.t ;
  fees : Tez.t ;
  gas : Z.t ;
}

let create_forged_mgr_op ~payload ~fees ~gas =
  { payload ; fees ; gas }

let forged_mgr_op_encoding =
  let open Data_encoding in
  conv
    (fun { payload ; fees ; gas } -> (payload, fees, gas))
    (fun (payload, fees, gas) -> { payload ; fees ; gas })
    (obj3
       (req "result" bytes)
       (req "total_fee" Tez.encoding)
       (req "total_gas" n))

let forge_manager_operation
    ?(fee_parameter=default_fee_parameter) ?pk cctxt source pack_op ops =
  Alpha_services.Contract.manager_key
    cctxt (`Main, `Head 0) source >>=? fun (_pkh, maybe_pk) ->
  Alpha_services.Contract.counter
    cctxt (`Main, `Head 0) source >>=? fun counter ->
  let counter = Z.succ counter in
  fold_left_s begin fun (counter, a) spec ->
    let reveal = match a, maybe_pk, pk with
      | [], None, Some pk -> Some pk
      | [], None, None ->
        invalid_arg "forge_manager_operation: reveal needed and no pk \
                     provided"
      | _ -> None in
    pack_op ?reveal ~source ~counter spec >>=? fun (packed_contents,
                                                    counter) ->
    let c = Alpha_context.Operation.to_list packed_contents in
    return (counter, List.rev_append c a)
  end (counter, []) ops >>=? fun (_, content_list) ->
  let packed_content_list =
    Alpha_context.Operation.of_list (List.rev content_list) in
  patch_fees ~fee_parameter
    cctxt packed_content_list >>=? fun (packed_content_list, fees, gas) ->
  Alpha_block_services.hash cctxt () >>=? fun branch ->
  let shell_header = Tezos_base.Operation.{ branch } in
  let unsigned_operation = shell_header, packed_content_list in
  let unsigned_op_json =
    Data_encoding.Json.construct
      Alpha_context.Operation.unsigned_encoding unsigned_operation in
  Logs.app ~src (fun m -> m "%s" (Data_encoding.Json.to_string unsigned_op_json)) ;
  begin try
      let payload = Data_encoding.Binary.to_bytes_exn
          Alpha_context.Operation.unsigned_encoding
          unsigned_operation in
      return (create_forged_mgr_op ~payload ~fees ~gas)
    with
    | Data_encoding.Binary.Write_error err as exn ->
      Logs.err ~src (fun m -> m "Error while binary encoding: %a"
                        Data_encoding.Binary.pp_write_error err) ;
      Lwt.return (error_exn exn)
  end

let forge_transfer ?fee_parameter ?pk cctxt source dests =
  forge_manager_operation ?fee_parameter ?pk cctxt source pack_tx dests

type origination = {
  manager: Signature.Public_key_hash.t ;
  delegate: Signature.Public_key_hash.t option ;
  script: Script.t option ;
  credit: Tez.t ;
  delegatable: bool ;
}

let origination_encoding =
  let open Data_encoding in
  conv
    (fun { manager ; delegate ; script ; credit ; delegatable } ->
       (manager, delegate, script, credit, delegatable))
    (fun (manager, delegate, script, credit, delegatable) ->
       { manager ; delegate ; script ; credit ; delegatable })
    (obj5
       (req "manager" Signature.Public_key_hash.encoding)
       (opt "delegate" Signature.Public_key_hash.encoding)
       (opt "script" Script.encoding)
       (req "credit" Tez.encoding)
       (dft "delegatable" bool true))

let pack_origination ?reveal ~source ~counter
    { manager ; delegate ; script ; credit ; delegatable } =
  let operation = Alpha_context.Origination {
      manager ; delegate ; script ;
      spendable = true ;
      delegatable ;
      credit ;
      preorigination = None
    } in
  pack_mgr_operation ?reveal ~source ~counter ~operation

let forge_origination ?fee_parameter cctxt source pk origs =
  forge_manager_operation
    ?fee_parameter cctxt source ~pk pack_origination origs

let forge_delegation ?fee_parameter cctxt source pk delegation =
  forge_manager_operation
    ?fee_parameter cctxt source ~pk pack_delegation [ delegation ]

(*---------------------------------------------------------------------------
   Copyright (c) 2018 Vincent Bernardoff

   Permission to use, copy, modify, and/or distribute this software for any
   purpose with or without fee is hereby granted, provided that the above
   copyright notice and this permission notice appear in all copies.

   THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
   WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
   MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
   ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
   WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
   ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
   OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
  ---------------------------------------------------------------------------*)
