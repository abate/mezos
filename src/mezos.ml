(*---------------------------------------------------------------------------
   Copyright (c) 2018 Vincent Bernardoff. All rights reserved.
   Distributed under the ISC license, see terms at the end of the file.
  ---------------------------------------------------------------------------*)

open Cmdliner
open Alpha_cmdliner_helpers

module Sqlexpr = Sqlexpr_sqlite_lwt

let src =
  Logs.Src.create ~doc:"Main Mezos daemon" "mezos"
let log_src = src

let default_remote_host = "localhost"
let default_tezos_rpc_port = 8732
let default_tezos_zeronet_rpc_port = 18732
let default_mezos_rpc_port = 8776

let default_tezos_zeronet_url =
  Uri.make ~scheme:"http" ~host:default_remote_host ~port:default_tezos_zeronet_rpc_port ()
let default_tezos_url =
  Uri.make ~scheme:"http" ~host:default_remote_host ~port:default_tezos_rpc_port ()
let default_mezos_url =
  Uri.make ~scheme:"http" ~host:default_remote_host ~port:default_mezos_rpc_port ()

let find_pkh
    (extended_pk : Monocypher.public Monocypher.Sign.key Bip32_ed25519.t)
    pkh =
  let pkh_of_bip32 pk =
    let open Monocypher in
    let buf = Bigstring.create (1 + Sign.length pk) in
    Bigstring.set buf 0 '\x00' ;
    let _nb_written = Monocypher.Sign.blit pk buf 1 in
    let pk = Data_encoding.Binary.of_bytes_exn
        Signature.Public_key.encoding buf in
    let pkh = Signature.Public_key.hash pk in
    pk, pkh in
  let rec inner node =
    match Bip32_ed25519.derive (module Wallet_helpers.Crypto) extended_pk node with
    | None -> inner (Int32.succ node)
    | Some extended_pk ->
      let pk = Bip32_ed25519.key extended_pk in
      let derived_pk, derived_pkh = pkh_of_bip32 pk in
      if derived_pkh = pkh then begin
        Logs.debug ~src (fun m -> m "Found %a at node %ld"
                       Signature.Public_key_hash.pp pkh
                       node) ;
        node, pk, derived_pk
      end
      else inner (Int32.succ node)
  in return (inner 0l)

let build_pk pk =
  let open Monocypher in
  let buf = Bigstring.create (1 + Sign.length pk) in
  Bigstring.set buf 0 '\x00' ;
  let _nb_written = Sign.blit pk buf 1 in
  Data_encoding.Binary.of_bytes_exn Signature.Public_key.encoding buf

let cleanup_files = ref []
let () =
  Sys.set_signal Sys.sigint begin Signal_handle begin fun _ ->
      List.iter begin fun fn ->
        Logs.info ~src (fun m -> m "Cleaned up %s" fn) ;
        Unix.unlink fn ;
      end !cleanup_files ;
      exit 0
    end
  end

let sign_and_inject_payload ?epk cctxt esk path payload =
  let open Bip32_ed25519 in
  let derived_esk =
    key (derive_path_exn (module Wallet_helpers.Crypto) esk path) in
  let derived_epk =
    Option.map epk ~f:begin fun epk ->
      key (derive_path_exn (module Wallet_helpers.Crypto) epk path)
    end in
  let preimage =
    Blake2B.(to_bytes (hash_bytes [ MBytes.of_string "\x03" ; payload ])) in
  let signature = MBytes.create Monocypher.Sign.bytes in
  let _nb_written = Monocypher.Sign.sign_extended ?pk:derived_epk
      ~ek:derived_esk ~msg:preimage signature in
  Monocypher.Sign.wipe derived_esk ;
  Option.iter ~f:Monocypher.Sign.wipe derived_epk ;
  Injection_services.operation
    cctxt (MBytes.concat "" [payload; signature]) >>=? fun oh ->
  Logs.app ~src (fun m -> m "Injected %a" Operation_hash.pp oh) ;
  return oh

let transfer tezos_client_dir datadir tezos_url alias path dsts () =
  let dsts = List.map begin fun (destination, amount) ->
      Mezos_transfer.create_destination ~destination ~amount ()
    end dsts in
  Db.open_or_init_db ~datadir (module Wallet_db) >>= fun (db, _inited) ->
  Tezos_cfg.mk_rpc_cfg tezos_client_dir tezos_url >>=? fun (rpc_config, confirmations) ->
  let cctxt = new wrap_full (Tezos_cfg.mezos_full ~rpc_config ?confirmations ()) in
  Wallet_helpers.get_wallet_extended_sk db alias >>= fun extended_sk ->
  let derived_extended_sk =
    Bip32_ed25519.derive_path_exn (module Wallet_helpers.Crypto) extended_sk path in
  Bip32_ed25519.wipe extended_sk ;
  let epk = Bip32_ed25519.neuterize derived_extended_sk in
  let pk = build_pk (Bip32_ed25519.key epk) in
  let pkh = Signature.Public_key.hash pk in
  let source = Alpha_context.Contract.implicit_contract pkh in
  Mezos_transfer.forge_transfer cctxt source ~pk dsts >>=? fun { payload ; _ } ->
  sign_and_inject_payload cctxt derived_extended_sk [] payload >>=? fun _oh ->
  return_unit

module IntSet = Set.Make(Int32)

let missing_blocks db =
  Chain_db.fold_levels db begin fun a level _hash ->
    Lwt.return (IntSet.add level a)
  end IntSet.empty >>= fun levels ->
  match IntSet.max_elt_opt levels with
  | None -> Lwt.return IntSet.empty
  | Some max_elt ->
    let rec acc_missing missing i =
      if i >= max_elt then missing
      else
      if not (IntSet.mem i levels) then
        acc_missing (IntSet.add i missing) (Int32.succ i)
      else missing in
    Lwt.return (acc_missing IntSet.empty 3l)

module Directories = struct
  let derive_key extended_sk dir =
    let open RPC_directory in
    register1 dir Mezos_services_common.derive_key begin fun path () () ->
      let _pk, pkh = Wallet_helpers.ed25519_pkh_of_extended extended_sk ~path in
      return pkh
    end

  let forge_transfer cctxt dir =
    let open RPC_directory in
    register0 dir Mezos_services.forge_transfer
      begin fun () ({ src; src_pk; ops } as t) ->
        Logs_lwt.info begin fun m -> m "<- %a"
            Mezos_transfer.(pp_manager_operations pp_destination) t
        end >>= fun () ->
        Mezos_transfer.forge_transfer cctxt src ~pk:src_pk ops
      end

  let originate_account cctxt dir =
    let open RPC_directory in
    register0 dir Mezos_services.originate_account
      begin fun () { src; src_pk; ops } ->
        Mezos_transfer.forge_origination cctxt src src_pk ops
    end

  let change_delegate cctxt dir =
    let open RPC_directory in
    register0 dir Mezos_services.change_delegate
      begin fun () { src; src_pk; ops } ->
        match ops with
        | [] ->  Mezos_transfer.forge_delegation cctxt src src_pk None
        | pkh :: _ -> Mezos_transfer.forge_delegation cctxt src src_pk (Some pkh)
      end

  let history chain_db dir =
    let open RPC_directory in
    register0 dir Mezos_services_common.history begin fun ks () ->
      Chain_db.history chain_db ks >>|? fun txs ->
      List.map begin fun txs ->
        Operation_hash.Map.fold begin fun _oh os a ->
          IntMap.fold begin fun _op_id tx a ->
            tx :: a
          end os a
        end txs []
      end txs
    end

  let contracts chain_db dir =
    let open RPC_directory in
    register0 dir Mezos_services_common.contracts begin fun pkhs () ->
      Chain_db.ks_of_mgr chain_db pkhs >>= fun kss ->
      return kss
    end

  let contract_info chain_db dir =
    let open RPC_directory in
    register0 dir Mezos_services_common.contract_info begin fun ks () ->
      Lwt_list.map_s (Chain_db.get_contract_info chain_db) ks >>= fun infos ->
      return infos
    end

  let missing_blocks chain_db dir =
    let open RPC_directory in
    register0 dir Mezos_services_common.missing_blocks begin fun () () ->
      missing_blocks chain_db >>= fun missing ->
      return (IntSet.elements missing)
    end

  let stake chain_db dir =
    let open RPC_directory in
    register2 dir Mezos_services_common.stake begin fun delegate k () () ->
      Chain_db.fold_stake chain_db ~delegate ~k
        ~init:[] ~f:begin fun a ~level ~kind ~amount ->
        Lwt.return (Mezos_stake.create_fulldiff ~level ~kind ~amount :: a)
      end >>= fun evts ->
      return (List.rev evts)
    end
end

let run tezos_url tezos_client_dir datadir mezos_url alias () =
  Sqlexpr.set_retry_on_busy true ;
  Db.open_or_init_db ~datadir (module Wallet_db) >>= fun (wallet_db, _inited) ->
  Db.open_or_init_db ~datadir (module Chain_db) >>= fun (chain_db, _inited) ->
  begin match alias with
  | None -> return_none
  | Some alias ->
    Wallet_helpers.get_wallet_extended_sk wallet_db alias >>= fun extended_sk ->
    let _root_pk, root_pkh = Wallet_helpers.ed25519_pkh_of_extended extended_sk in
    Logs.info ~src (fun m -> m "Root PKH: %a" Ed25519.Public_key_hash.pp root_pkh) ;
    return_some extended_sk
  end >>=? fun extended_sk ->
  Tezos_cfg.mk_rpc_cfg
    tezos_client_dir tezos_url >>=? fun (rpc_config, confirmations) ->
  let cctxt = new wrap_full (Tezos_cfg.mezos_full ~rpc_config ?confirmations ()) in
  let mezos_url =
    Option.unopt ~default:default_mezos_url mezos_url in
  let host = Option.unopt ~default:"localhost" (Uri.host mezos_url) in
  let mezos_port = Option.unopt ~default:8776 (Uri.port mezos_url) in
  RPC_server.launch ~host ~media_types:Media_type.all_media_types (`TCP (`Port mezos_port))
    begin
      RPC_directory.register_describe_directory_service begin
        RPC_directory.empty |>
        (match extended_sk with
         | None -> (fun d -> d)
         | Some esk -> Directories.derive_key esk) |>
        Directories.forge_transfer cctxt |>
        Directories.originate_account cctxt |>
        Directories.change_delegate cctxt |>
        Directories.contract_info chain_db |>
        Directories.history chain_db |>
        Directories.contracts chain_db |>
        Directories.missing_blocks chain_db |>
        Directories.stake chain_db
      end
        RPC_service.description_service
    end
  >>= fun _server ->
  (* Optimize the DB every hour *)
  let rec optimize_background () =
    Lwt_unix.sleep 3600. >>= fun () ->
    Sqlexpr.execute chain_db Db.optimize_pragma >>=
    optimize_background in
  Lwt.async optimize_background ;
  let th, _u = Lwt.wait () in
  th >>= fun () ->
  return_unit

let h = Bip32_ed25519.to_hardened

let descent cctxt extended_sk =
  let path_of_account_node ~account ~node =
    [h 44l ; h 1729l ; h (Int32.of_int account) ; Int32.of_int node ] in
  let rec inner account node =
    let path = path_of_account_node ~account ~node in
    let _pk, pkh = Wallet_helpers.pkh_of_extended ~path extended_sk in
    let contract = Alpha_context.Contract.implicit_contract pkh in
    Alpha.Context.get_balance cctxt
      ~chain:`Main ~block:(`Head 0) contract >>=? fun balance ->
    match account, node, Alpha_context.Tez.(balance = zero) with
    | _, 0, true ->
      Logs.app ~src (fun m -> m "%a (%a) has 0 tz, exiting"
                   Signature.Public_key_hash.pp pkh
                   Bip32_ed25519.Human_readable.pp_path path) ;
      return_unit
    | _, addr, false ->
      Logs.app ~src (fun m -> m "%a (%a) has %a tz"
                   Signature.Public_key_hash.pp pkh
                   Bip32_ed25519.Human_readable.pp_path path
                   Alpha_context.Tez.pp balance) ;
      inner account (succ addr)
    | account, _, true ->
      Logs.app ~src (fun m -> m "%a (%a) has 0 tz, skip to next account"
                   Signature.Public_key_hash.pp pkh
                   Bip32_ed25519.Human_readable.pp_path path) ;
      inner (succ account) 0
  in
  inner 0 0

let balance tezos_client_dir datadir url alias () =
  Db.open_or_init_db ~datadir (module Wallet_db) >>= fun (db, _inited) ->
  Tezos_cfg.mk_rpc_cfg
    tezos_client_dir url >>=? fun (rpc_config, confirmations) ->
  let cctxt = new wrap_full (Tezos_cfg.mezos_full ~rpc_config ?confirmations ()) in
  Wallet_helpers.get_wallet_extended_sk db alias >>= fun extended_sk ->
  descent cctxt extended_sk

let accounts datadir () =
  Db.open_or_init_db ~datadir (module Chain_db) >>= fun (chain_db, _chain_inited) ->
  Chain_db.fold_contracts chain_db begin fun () k mgr ->
    Logs.app ~src begin fun m -> m "%a %a"
        Chain_db.AC.Contract.pp k
        Signature.Public_key_hash.pp mgr
    end ;
    Lwt.return_unit
  end () >>= fun () ->
  return_unit

let remote_transfer tezos_client_dir datadir tezos_url alias base src dsts () =
  let base = Option.unopt ~default:default_mezos_url base in
  Db.open_or_init_db ~datadir (module Wallet_db) >>= fun (db, _inited) ->
  Tezos_cfg.mk_rpc_cfg
    tezos_client_dir tezos_url >>=? fun (rpc_config, confirmations) ->
  let cctxt = new wrap_full (Tezos_cfg.mezos_full ~rpc_config ?confirmations ()) in
  Client_proto_contracts.get_manager
    cctxt ~chain:`Main ~block:(`Head 0) src >>=? fun src_pkh ->
  Wallet_helpers.get_wallet_extended_sk db alias >>= fun extended_sk ->
  let extended_sk = Bip32_ed25519.derive_path_exn (module Wallet_helpers.Crypto)
      extended_sk [h 44l ; h 1729l ; h 0l] in
  let extended_pk = Bip32_ed25519.neuterize extended_sk in
  find_pkh extended_pk src_pkh >>=? fun (node, _ek, src_pk) ->
  let dsts =
    List.map begin fun (destination, amount) ->
      Mezos_transfer.create_destination ~destination ~amount ()
    end dsts in
  let transfer = Mezos_transfer.create_transfer ~src ~src_pk ~dsts in
  RPC_client.call_service
    Media_type.all_media_types ~base Mezos_services.forge_transfer
    () () transfer >>=? fun { payload ; fees ; gas } ->
  sign_and_inject_payload
    cctxt ~epk:extended_pk extended_sk [ node ] payload >>= function
  | Ok op_hash ->
    Logs.app ~src:log_src begin fun m ->
      m "Injected operation %a with fees %a and gas %a"
        Operation_hash.pp op_hash Alpha_context.Tez.pp fees
        Z.pp_print gas
    end ;
    return_unit
  | Error e ->
    Logs.err ~src:log_src (fun m -> m "%a" pp_print_error e) ;
    return_unit

let remote_history base srcs max_level () =
  let base =
    Option.unopt ~default:default_mezos_url base in
  RPC_client.call_service Media_type.all_media_types
    ~base Mezos_services_common.history () srcs () >>=? fun txs ->
  List.iter2 begin fun cur_src txs ->
    let module KMap = Map.Make(Chain_db.AC.Contract) in
    let summary =
      List.fold_left begin fun a { Chain_db.src; dst; amount; level; _ } ->
        let open Chain_db.AC in
        if level > max_level then a else
          match cur_src = src, cur_src = dst with
          | true, true -> a (* delegate to itself *)
          | true, false -> (* out *)
            KMap.update dst begin function
              | None -> (match Tez.(zero -? amount) with Ok v -> Some v | _ -> None)
              | Some c -> (match Tez.(c -? amount) with Ok v -> Some v | _ -> None)
            end a
          | false, true -> (* in *)
            KMap.update src begin function
              | None -> Some amount
              | Some c -> (match Tez.(c +? amount) with Ok v -> Some v | _ -> None)
            end a
          | false, false ->
            invalid_arg "remote_history"
      end KMap.empty txs in
    Logs.app ~src (fun m -> m "For %a" Chain_db.AC.Contract.pp cur_src) ;
    let sum =
      let open Chain_db.AC in
      KMap.fold (fun _ t a -> Int64.add a (Tez.to_mutez t)) summary 0L in
    KMap.iter begin fun k t ->
      let open Chain_db.AC in
      let part = Tez.to_mutez t in
      let p = Int64.(to_float part /. to_float sum) in
      Logs.app ~src (fun m -> m "%a %a %f" Contract.pp k Tez.pp t p)
    end summary
  end srcs txs ;
  return_unit

let init_db datadir () =
  Db.open_or_init_db ~datadir (module Wallet_db) >>= fun (_wallet_db, wallet_inited) ->
  Db.open_or_init_db ~datadir (module Chain_db) >>= fun (_chain_db, chain_inited) ->
  Logs.app ~src (fun m -> m "Wallet DB inited: %b " wallet_inited) ;
  Logs.app ~src (fun m -> m "Chain DB inited: %b " chain_inited) ;
  return ()

let init_db_cmd =
  let open Cmdliner_helpers.Terms in
  let doc = "Initialize Mezos DB." in
  Term.(const init_db $ datadir $ setup_log),
  Term.info ~doc "init-db"

let blkid_of_hash_level hash level =
  `Main, `Hash (hash, level)

open Cmdliner_helpers.Convs
open Cmdliner_helpers.Terms
module OldConvs = Old_alpha_cmdliner_helpers.Convs
module OldTerms = Old_alpha_cmdliner_helpers.Terms
module NewConvs = Alpha_cmdliner_helpers.Convs
module NewTerms = Alpha_cmdliner_helpers.Terms

let run_cmd =
  let doc = "Run a Mezos node." in
  let tezos_url =
    uri ~args:["tezos-url"] ~doc:"URL of a running Tezos node" in
  let mezos_url =
    uri ~args:["mezos-url"] ~doc:"URL to bind Mezos" in
  Term.(const run $ tezos_url
        $ tezos_client_dir $ datadir $ mezos_url
        $ alias_opt $ setup_log),
  Term.info ~doc "run"

let balance_cmd =
  let doc = "Balance of an account" in
  Term.(const balance $ tezos_client_dir $ datadir $
        uri ~args:["tezos-url"] ~doc:"URL of a Tezos node" $
        (alias_pos 0) $ setup_log),
  Term.info ~doc "balance"


let history_cmd =
  let doc = "History of an account" in
  let ks =
    Arg.(value & pos_right ~-1 OldConvs.contract [] &
         info [] ~docv:"CONTRACT" ~doc) in
  let history datadir ks () =
    Db.open_or_init_db ~datadir (module Chain_db) >>= fun (chain_db, _chain_inited) ->
    Chain_db.history ~display:true chain_db ks >>|? ignore in
  Term.(const history $ datadir $ ks $ setup_log),
  Term.info ~doc "history"

let history_remote_cmd =
  let doc = "History of an account using RPC calls" in
  let max_level =
    Arg.(value & opt int max_int & info ["max-level"] ~docv:"LEVEL") in
  let src =
    Arg.(value & pos_right ~-1 OldConvs.contract [] & info [] ~docv:"SRC") in
  Term.(const remote_history $
        uri ~args:["mezos-url"] ~doc: "URL of a Mezos server" $
        src $ max_level $ setup_log),
  Term.info ~doc "history-remote"

let accounts_cmd =
  let doc = "List all accounts on the blockchain" in
  Term.(const accounts $ datadir $ setup_log),
  Term.info ~doc "accounts"

let transfer_cmd =
  let doc = "Transfer XTZ" in
  let path =
    Arg.(required & pos 1 (some bip32_path) None & info [] ~docv:"BIP32_PATH") in
  let dsts =
    let open NewConvs in
    Arg.(value & pos_right 1 (t2 contract tez) [] & info [] ~docv:"TX_SPEC") in
  Term.(const transfer $ tezos_client_dir $ datadir $
        uri ~args:["u"; "tezos-url"] ~doc:"URL of a Tezos node" $
        (alias_pos 0) $ path $ dsts $ setup_log),
  Term.info ~doc "transfer"

let remote_transfer_cmd =
  let open NewConvs in
  let doc = "Transfer XTZ using RPC calls" in
  let src =
    Arg.(required & pos 1 (some contract) None & info [] ~docv:"SRC") in
  let dsts =
    Arg.(value & pos_right 1 (t2 contract tez) [] & info [] ~docv:"TX_SPEC") in
  Term.(const remote_transfer $ tezos_client_dir $ datadir $
        uri
          ~args:["tezos-url"]
          ~doc: "URL of a Tezos server" $
        (alias_pos 0) $ uri
          ~args:["mezos-url"]
          ~doc: "URL of a Mezos server" $
        src $ dsts $ setup_log),
  Term.info ~doc "transfer-remote"

let lwt_run v =
  Lwt.async_exception_hook := begin fun exn ->
    Logs.err ~src (fun m -> m "%a" pp_exn exn) ;
  end ;
  match Lwt_main.run v with
  | Error err ->
    Logs.err ~src (fun m -> m "%a" pp_print_error err) ;
    exit 1
  | Ok () -> ()

let cmds =
  List.map begin fun (term, info) ->
    Term.((const lwt_run) $ term), info
  end [
    init_db_cmd ;
    balance_cmd ;
    accounts_cmd ;
    history_cmd ;
    history_remote_cmd ;
    transfer_cmd ;
    remote_transfer_cmd ;
    run_cmd ;
  ]

let default_cmd =
  let doc = "Mezos: high level node/client on top of Tezos" in
  Term.(ret (const (`Help (`Pager, None)))),
  Term.info ~doc "mezos"

let () = match Term.eval_choice default_cmd cmds with
  | `Error _ -> exit 1
  | #Term.result -> exit 0

(*---------------------------------------------------------------------------
   Copyright (c) 2018 Vincent Bernardoff

   Permission to use, copy, modify, and/or distribute this software for any
   purpose with or without fee is hereby granted, provided that the above
   copyright notice and this permission notice appear in all copies.

   THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
   WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
   MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
   ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
   WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
   ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
   OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
  ---------------------------------------------------------------------------*)
