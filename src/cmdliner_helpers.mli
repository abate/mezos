(*---------------------------------------------------------------------------
   Copyright (c) 2018 Vincent Bernardoff. All rights reserved.
   Distributed under the ISC license, see terms at the end of the file.
  ---------------------------------------------------------------------------*)

open Cmdliner

module Convs : sig
  val uri : Uri.t Arg.conv
  val bip32_node : int32 Arg.conv
  val bip32_path : int32 list Arg.conv
end

module Terms : sig
  val setup_log : unit Term.t
  val tezos_client_dir : string Term.t
  val datadir : string Term.t
  val force : doc:string -> bool Term.t
  val cfg_file : string Term.t

  val host :
    ?argnames:string list ->
    default:string -> doc:string -> unit -> string Term.t
  val port :
    ?argnames:string list ->
    default:int -> doc:string -> unit -> int Term.t
  val uri :
    doc:string -> args:string list -> Uri.t option Term.t
  val ovh_log : (Uri.t * string) option Term.t
  val tls : bool Term.t

  val alias_pos : int -> string Term.t
  val alias_opt : string option Term.t
end

(*---------------------------------------------------------------------------
   Copyright (c) 2018 Vincent Bernardoff

   Permission to use, copy, modify, and/or distribute this software for any
   purpose with or without fee is hereby granted, provided that the above
   copyright notice and this permission notice appear in all copies.

   THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
   WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
   MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
   ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
   WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
   ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
   OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
  ---------------------------------------------------------------------------*)
