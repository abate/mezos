val base_dir : string
(** [base_dir] is $HOME/.tezos-client *)

val mk_rpc_cfg :
  string -> Uri.t option -> (RPC_client.config * int option) tzresult Lwt.t
(** [read_cfg_file tezos_client_dir url] is (rpc_cfg, confirmations)
    where the config is either deduced from [url], which is typically
    passed as a cmdline argument, or from [tezos_client_dir] where a
    cfg file is expected to be present. *)

val mezos_full :
  ?block:Block_services.block ->
  ?confirmations:int ->
  ?password_filename:string ->
  ?base_dir:string ->
  ?rpc_config:RPC_client.config -> unit -> Client_context.full
