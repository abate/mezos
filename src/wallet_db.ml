(*---------------------------------------------------------------------------
   Copyright (c) 2018 Vincent Bernardoff. All rights reserved.
   Distributed under the ISC license, see terms at the end of the file.
  ---------------------------------------------------------------------------*)

module Sqlexpr = Sqlexpr_sqlite_lwt

let db_name = "wallet.db"

let _ = [
  [%sqlinit "drop table if exists wallet"] ;
  [%sqlinit "create table wallet (alias text primary key, \
             salt blob not null, \
             seed blob not null) without rowid"] ;
]

let update_seed_stmt =
  [%sqlc "insert or replace into wallet values (%s, %S, %S)"]
let delete_seed_stmt =
  [%sqlc "delete from wallet where alias = %s"]
let wallet_list_stmt =
  [%sqlc "select @s{alias} from wallet"]
let wallet_find_stmt =
  [%sqlc "select @s{salt}, @s{seed} from wallet where alias = %s"]

let auto_init_db, check_db, auto_check_db = [%sqlcheck "sqlite"]

open Lwt.Infix

let aliases db =
  Sqlexpr.fold db begin fun acc alias ->
    Lwt.return (alias :: acc)
  end [] wallet_list_stmt >>= fun aliases ->
  Lwt.return aliases

(*---------------------------------------------------------------------------
   Copyright (c) 2018 Vincent Bernardoff

   Permission to use, copy, modify, and/or distribute this software for any
   purpose with or without fee is hereby granted, provided that the above
   copyright notice and this permission notice appear in all copies.

   THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
   WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
   MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
   ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
   WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
   ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
   OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
  ---------------------------------------------------------------------------*)

