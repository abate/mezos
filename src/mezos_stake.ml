module Sqlexpr = Sqlexpr_sqlite_lwt

module KMap = Map.Make(Alpha_context.Contract)
module Int32Map = Map.Make(Int32)

let src = Logs.Src.create "mezos.stake"

type diff = {
  amount : int64 ;
  kind : [`Contract | `Rewards] ;
}

let kind_encoding =
  let open Data_encoding in
  string_enum [
    "contract", `Contract ;
    "reward", `Rewards
  ]

let diff_encoding =
  let open Data_encoding in
  conv
    (fun { amount ; kind } -> (amount, kind))
    (fun (amount, kind) -> { amount ; kind })
    (obj2
       (req "amount" int64)
       (req "kind" kind_encoding))

type fulldiff = {
  level : int32 ;
  diff : diff ;
}

let create_fulldiff ~level ~kind ~amount = {
  level ; diff = { amount ; kind }
}

let fulldiff_encoding =
  let open Data_encoding in
  conv
    (fun { level ; diff } -> (level, diff))
    (fun (level, diff) -> { level ; diff })
    (merge_objs
       (obj1 (req "level" int32)) diff_encoding)

let bal amount = { amount ; kind = `Contract }
let rew amount = { amount ; kind = `Rewards }

type t = (int64 * (diff list)) Int32Map.t KMap.t

exception Abort of int64

let balance_at_level s ~k ~level =
  match KMap.find_opt k s with
  | None -> 0L
  | Some m -> try
      Int32Map.fold begin fun lvl (sum, _) a ->
        if lvl > level then raise (Abort a) ;
        Int64.add sum a
      end m 0L
    with Abort sum -> sum

let empty = KMap.empty

let add_diff s k level ({ amount ; _ } as di) =
  KMap.update k begin function
    | None ->
      Some (Int32Map.singleton level (amount, [di]))
    | Some diffs ->
      Some (Int32Map.update level begin function
          | None ->
            Some (amount, [di])
          | Some (prev_amount, dis) ->
            Some (Int64.add amount prev_amount, di :: dis)
        end diffs)
  end s

let update ~db ~delegate s level k ({ amount ; kind } as diff) =
  let open Alpha_context in
  Chain_db.update_stake ~delegate ~level ~k ~amount ~kind db >|= fun () ->
  add_diff s k level diff

let pp_cat ppf = function
  | Alpha_context.Delegate.Contract _k -> Format.pp_print_string ppf "Contract"
  | Rewards _ -> Format.pp_print_string ppf "Rewards"
  | Fees _ -> Format.pp_print_string ppf "Fees"
  | Deposits _ -> Format.pp_print_string ppf "Deposits"

let process
    ?balances ~db ~tx_map ~pkh ~k ~stake
    { Chain_db.level ; cycle ; cycle_position ; cat; op ; diff ; _ } =
  let open Alpha_context in
  Logs_lwt.debug ~src begin fun m ->
    m "process: lvl %ld amount %a %a"
      level Tez.pp (Obj.magic diff) pp_cat cat
  end >>= fun () ->
  let update = update ~db ~delegate:pkh in
  let share_reward stake level balances diff =
    Lwt_list.fold_left_s begin fun s (k, stake) ->
      let reward = Int64.to_float diff *. stake in
      let reward_i = Int64.of_float reward in
      if reward_i < 1L then (* do not record sub-mutez rewards *)
        Lwt.return s
      else
        update s level k (rew reward_i) >>= fun t ->
        Logs_lwt.info ~src begin fun m ->
          let bal = balance_at_level ~k ~level t in
          m "%ld %a %f %.6f %a"
            level Contract.pp k stake (reward /. 1e6)
            Tez.pp (Obj.magic bal)
        end >>= fun () ->
        Lwt.return t
    end stake balances in
  match cat, op with
  | Delegate.Fees (_, _), None ->
    (* Fees outside operation. Summary of all fees earned in the last
       5 cycles. They have already been counted at the time they have
       been earned. *)
    Lwt.return stake
  | Fees (_, _), Some _ -> begin
      (* Fees inside operation, negligible but share anyway *)
      match balances with
      | None -> Lwt.return stake
      | Some b -> share_reward stake level b diff
    end
  | Rewards _, Some _ ->
    (* Rewards marked at endorsement or baking time. Since they count
       as stake only at unlock time, we share them at that time. *)
    Lwt.return stake
  | Rewards _, None when cycle_position = 4095l -> begin
      (* Distribute aggregated cycle rewards. Also display statistics
         of pool members. *)
      let diff = Int64.neg diff in (* necessary because marked negative *)
      match balances with
      | None -> assert false
      | Some b ->
        Logs_lwt.info ~src begin fun m ->
          m "Reward unlocked at cycle %a. Sharing %a tz"
            Cycle.pp cycle Tez.pp (Obj.magic diff)
        end >>= fun () ->
        share_reward stake level b diff >>= fun stake ->
        (* End of cycle: display summary. *)
        KMap.iter begin fun k _ ->
          Logs.info ~src begin fun m ->
            m "* %a has %a tz" Contract.pp k Tez.pp
              (Obj.magic (balance_at_level stake ~k ~level))
          end
        end stake ;
        Lwt.return stake
    end
  | Rewards _, None ->
    (* Block rewards marked when they are baked. We share at unlocking
       time, see clause above. *)
    Lwt.return stake
  | Delegate.Deposits (_, _), _ ->
    (* Debited to pay the bond of endorsements. We don't track them,
       neither when we pay them, or when we get them back. *)
    Lwt.return stake
  | Delegate.Contract _, None ->
    (* Debited to pay the bond of a block or credited at end of
       cycle. We don't track. *)
    Lwt.return stake
  | Delegate.Contract _, Some (oph, op_id) ->
    match Operation_hash.Map.find_opt oph tx_map with
    | None ->
      (* Not in the TX map: endorsement bond *)
      Lwt.return stake
    | Some o ->
      match IntMap.find_opt op_id o with
      | None -> assert false
      | Some o ->
        Chain_db.nb_alpha_operations db oph >>= fun nb_sub_ops ->
        begin match o.Chain_db.src = k, o.dst = k with
          | false, false -> assert false
          | true, true ->
            (* transaction to self, do nothing. *)
            Logs.info ~src begin fun m ->
              m "Transaction to self"
            end ;
            Lwt.return stake
          | false, true ->
            assert (diff > 0L) ;
            (* XTZ inflow: new staker or staker reinforcing its stake *)
            Logs.info ~src begin fun m ->
              m "%ld <- %a (%a tz)"
                level Contract.pp o.src Tez.pp (Obj.magic diff)
            end ;
            update stake level o.src (bal diff)
          | true, false ->
            assert (diff < 0L) ;
            (* XTZ outflow: paid rewards to delegators, leaving
               stakers *)
            Logs.info ~src begin fun m ->
              m "%ld -> %a (%a tz)"
                level Contract.pp o.src Tez.pp (Obj.magic (Int64.neg diff))
            end ;
            (* Update stakes only on leaving stakers. *)
            (* nb_sub_ops is a way to avoid counting delegate payments
               as a leaving staker for contracts that are both
               delegators and stakers. *)
            if KMap.mem o.dst stake && nb_sub_ops = 1 then
              update stake level o.dst (bal diff)
            else
              Lwt.return stake
        end

let recover_stake_db ?max_level db delegate =
  Chain_db.fold_stake_full ?max_level db ~delegate
    ~init:KMap.empty
    ~f:begin fun a ~k ~level ~kind ~amount ->
      Lwt.return (add_diff a k level { amount ; kind })
    end

exception Abort_build_stake of t

(* let cycle_pos_of_level l =
 *   let open Int32 in
 *   let pl = pred l in
 *   div pl 4096l, rem pl 4096l *)

let first_level_of_cycle l =
  let open Int32 in
  add 1l (mul l 4096l)

let build_stake_db
    db ?(related=[])
    ?(start_cycle=0l) ?(end_cycle=Int32.max_int) pkh =
  let open Alpha_context in
  let k = Contract.implicit_contract pkh in
  Lwt_list.fold_left_s begin fun a k ->
    Chain_db.find_txs_involving_k db k >|= fun txmap ->
    Operation_hash.Map.merge begin fun _ a b ->
      match a, b with
      | Some v, None -> Some v
      | None, Some v -> Some v
      | _ -> None
    end a txmap
  end Operation_hash.Map.empty (k :: related) >>= fun tx_map ->
  Chain_db.fold_delegate db ~pkh begin fun a di ->
    Lwt.return (Cycle.Map.add (Obj.magic di.Chain_db.cycle) di a)
  end Cycle.Map.empty >>= fun di_map ->
  let start_level = first_level_of_cycle start_cycle in
  recover_stake_db
    ~max_level:(Int32.pred start_level) db pkh >>= fun stake ->
  let fold_balances stake ({ Chain_db.cycle ; cat = _ ; _ } as bf) =
    if Cycle.to_int32 cycle >= end_cycle then raise (Abort_build_stake stake) ;
    match Cycle.Map.find_opt cycle di_map with
    | None ->
      (* Delegate does not exist yet, but we track movements. *)
      process ~db ~tx_map ~pkh ~k ~stake bf
    | Some { level ; staking ; _ } ->
      let stake, balances = KMap.fold begin fun k _ (stake, a) ->
          let kstake = balance_at_level ~k ~level stake in
          Logs.debug ~src begin fun m ->
            m "%a (%ld): %a / %a" Contract.pp k level
              Tez.pp (Obj.magic kstake)
              Tez.pp (Obj.magic staking)
          end ;
          (stake, (k, Int64.(to_float kstake /. to_float staking)) :: a)
        end stake (stake, []) in
      process ~db ~tx_map ~pkh ~k ~stake ~balances bf
  in
  Lwt.catch
    (fun () -> Chain_db.fold_balance_full db ~start_level ~k fold_balances stake)
    (function Abort_build_stake t -> Lwt.return t | exn -> Lwt.fail exn)
