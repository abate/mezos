module IntMap : Map.S with type key := int
module FloatMap : Map.S with type key := float

module Cache : sig
  type 'a t = {
    max_size : int;
    mutable cache : 'a FloatMap.t;
    mutable idx : int;
  }
  val create : ?max_size:int -> unit -> 'a t
  val find_opt : ('a * 'b) t -> 'a -> 'b option
  val add : ('a * 'b) t -> 'a -> 'b -> unit
end
