(*---------------------------------------------------------------------------
   Copyright (c) 2018 Vincent Bernardoff. All rights reserved.
   Distributed under the ISC license, see terms at the end of the file.
  ---------------------------------------------------------------------------*)

module Sqlexpr = Sqlexpr_sqlite_lwt

module type DB = sig
  val db_name : string
  val auto_init_db : Sqlite3.db -> Format.formatter -> bool
  val check_db : Sqlite3.db -> Format.formatter -> bool
  val auto_check_db : Format.formatter -> bool
end

val enable_foreign_keys_pragma :
  (unit Lwt.t, unit Lwt.t) Sqlexpr.statement

val optimize_pragma :
  (unit Lwt.t, unit Lwt.t) Sqlexpr.statement

val open_or_init_db :
  ?pragmas:(unit Lwt.t, unit Lwt.t) Sqlexpr.statement list ->
  datadir:string -> (module DB) -> (Sqlexpr.db * bool) Lwt.t

(*---------------------------------------------------------------------------
   Copyright (c) 2018 Vincent Bernardoff

   Permission to use, copy, modify, and/or distribute this software for any
   purpose with or without fee is hereby granted, provided that the above
   copyright notice and this permission notice appear in all copies.

   THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
   WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
   MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
   ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
   WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
   ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
   OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
  ---------------------------------------------------------------------------*)
