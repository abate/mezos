open Cmdliner
open Alpha_cmdliner_helpers

module Sqlexpr = Sqlexpr_sqlite_lwt

exception Exit_watch of error list

let src = Logs.Src.create "mezos.watch"

let watch tezos_url tezos_client_dir datadir blocks_per_sql_tx () =
  Sqlexpr.set_retry_on_busy true ;
  Db.open_or_init_db
    ~datadir (module Chain_db) >>= fun (chain_db, _inited) ->
  Tezos_cfg.mk_rpc_cfg
    tezos_client_dir tezos_url >>=? fun (rpc_config, confirmations) ->
  let cctxt =
    new wrap_full (Tezos_cfg.mezos_full ~rpc_config ?confirmations ()) in
  let target_blk, push_target_blk = Lwt.wait () in
  let start_watch, push_start_watch = Lwt.wait () in
  let watch () =
    let fold_f blk result =
      match blk, result with
      | _, Error err -> Lwt.fail (Exit_watch err)
      | Error err, _ ->
        Logs.err ~src (fun m -> m "%a" pp_print_error err) ;
        return_unit
      | Ok { Client_baking_blocks.hash }, Ok () ->
        Chain_db.store_blk_full
          ~block:(`Hash (hash, 0)) cctxt chain_db
    in
    Client_baking_blocks.monitor_valid_blocks
      cctxt ~next_protocols:None () >>=? fun stream ->
    Logs.debug ~src (fun m -> m "Started monitoring blocks") ;
    Lwt.catch begin fun () ->
      Lwt_stream.peek stream >>= function
      | None -> Lwt.fail (Exit_watch [])
      | Some (Error e)  -> Lwt.fail (Exit_watch e)
      | Some Ok { Client_baking_blocks.level ; _ } ->
        Lwt.wakeup
          push_target_blk (Alpha_context.Raw_level.to_int32 level) ;
        start_watch >>= fun () ->
        Lwt_stream.fold_s fold_f stream (Ok ())
    end begin function
      | Exit_watch err -> Lwt.return (Error err)
      | exn -> fail (Exn exn)
    end
  in
  let bootstrap () =
    target_blk >>= fun up_to ->
    Chain_db.bootstrap_chain ?blocks_per_sql_tx
      ~up_to cctxt chain_db >>=? fun _ ->
    Lwt.wakeup push_start_watch () ;
    return_unit in
  join [ watch () ; bootstrap () ]

let watch_cmd_lwt =
  let open Cmdliner_helpers.Terms in
  let blocks_per_sql_tx = Arg.(value & opt (some int32) None &
                               info ["save-increment"]
                                 ~doc:"How often to synchronize on disk") in
  let doc = "Follow Tezos blockchain and register new blocks." in
  Term.(const watch $
        uri ~args:["tezos-url"] ~doc:"URL of a running Tezos node" $
        tezos_client_dir $ datadir $ blocks_per_sql_tx $ setup_log),
  Term.info ~doc "mezos-watch"

let lwt_run v =
  Lwt.async_exception_hook := begin fun exn ->
    Logs.err ~src (fun m -> m "%a" pp_exn exn) ;
  end ;
  match Lwt_main.run v with
  | Error err ->
    Logs.err ~src (fun m -> m "%a" pp_print_error err) ;
    exit 1
  | Ok () -> ()

let cmd_of_lwt (term, info) =
  Term.((const lwt_run) $ term), info

let () = match Term.eval (cmd_of_lwt watch_cmd_lwt) with
  | `Error _ -> exit 1
  | #Term.result -> exit 0
