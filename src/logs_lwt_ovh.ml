(*---------------------------------------------------------------------------
   Copyright (c) 2019 Vincent Bernardoff. All rights reserved.
   Distributed under the ISC license, see terms at the end of the file.
  ---------------------------------------------------------------------------*)

open Lwt.Infix

let ovhtoken =
  Logs.Tag.def ~doc:"OVH id token" "X-OVH-TOKEN" Format.pp_print_string

let send_udp uri =
  let host =
    match Uri.host uri with
    | Some h -> h
    | None -> invalid_arg "send_udp: no host in URL" in
  let service =
    match Uri.port uri, Uri.scheme uri with
    | Some p, _ -> string_of_int p
    | None, Some s -> s
    | _ -> invalid_arg "send_udp: no port or scheme in URL" in
  let sock =
    match Unix.getaddrinfo host service
      [AI_SOCKTYPE SOCK_DGRAM] with
    | { ai_family ; ai_socktype ; ai_protocol ; ai_addr ; _ } :: _ ->
      let sock = Unix.socket ai_family ai_socktype ai_protocol in
      Unix.connect sock ai_addr ;
      Lwt_unix.of_unix_file_descr ~blocking:false sock
    | _ -> Pervasives.failwith "ovh_reporter: name resolve failed" in
    fun level msg ->
      let msg = Format.asprintf "%a@." Rfc5424.pp msg in
      begin match level with
        | Logs.App -> Lwt_io.(fprint stdout msg)
        | _ -> Lwt_io.(fprint stderr msg)
      end >>= fun () ->
      Lwt_unix.write sock
        (Bytes.unsafe_of_string msg) 0 (String.length msg) >>= fun _ ->
      Lwt.return_unit

let maybe_send f uri =
  match uri with
  | Some (uri, _) -> f uri
  | None ->
    fun level msg ->
      let msg = Format.asprintf "%a@." Rfc5424.pp msg in
      begin match level with
        | Logs.App -> Lwt_io.(fprint stdout msg)
        | _ -> Lwt_io.(fprint stderr msg)
      end

let warp10 (type a) (t:a Rfc5424.Tag.typ) (v:a) =
  match t with
  | Rfc5424.Tag.String -> Warp10.String v
  | Rfc5424.Tag.Bool   -> Warp10.Bool v
  | Rfc5424.Tag.Float  -> Warp10.Double v
  | Rfc5424.Tag.I64    -> Warp10.Long v
  | Rfc5424.Tag.U64    -> Warp10.Long (Uint64.to_int64 v)
  | Rfc5424.Tag.U      -> Warp10.Bool true

let push_tags defs tags p =
  let open Rfc5424 in
  List.iter begin fun ((Tag.Dyn (t, d)) as tydef) ->
    match Tag.find t tydef tags with
    | None -> ()
    | Some (_, None) -> ()
    | Some (_, Some v) ->
      p (Some (Warp10.create ~name:(Logs.Tag.name d) (warp10 t v)))
  end defs

let make_reporter ?(defs=[]) ?logs ?metrics f =
  let p =
    match metrics with
    | None -> None
    | Some (uri, token) ->
      let stream, stream_push = Lwt_stream.create () in
      Warp10_lwt.record ~uri ~token stream ;
      Some (stream, stream_push) in
  let send_metrics_from_tags tags =
    match p with
    | Some (stream, p) when (not (Lwt_stream.is_closed stream)) ->
      push_tags defs tags p
    | _ -> () in
  let tokens =
    match logs with
    | None -> Logs.Tag.empty
    | Some (_, token) -> Logs.Tag.(add ovhtoken token empty) in
  let tokens =
    if Logs.Tag.is_empty tokens then []
    else
      [Rfc5424.create_sd_element
         ~defs:[Rfc5424.Tag.string ovhtoken]
         ~section:"tokens" ~tags:tokens] in
  let hostname = Unix.gethostname () in
  let app_name = Filename.basename Sys.executable_name in
  let procid = string_of_int (Unix.getpid ()) in
  let pf = Rfc5424.create ~hostname ~procid in
  let report src level ~over k msgf =
    let m ?header:_ ?(tags=Logs.Tag.empty) fmt =
      let othertags =
        Rfc5424.create_sd_element ~defs ~section:"logs" ~tags in
      let structured_data =
        if Logs.Tag.is_empty tags
        then tokens
        else othertags :: tokens in
      let pf = pf
        ~severity:(Rfc5424.severity_of_level level)
        ~app_name:(app_name ^ "." ^ Logs.Src.name src)
        ~structured_data ~ts:(Ptime_clock.now ()) in
      let k msg =
        Lwt.async begin fun () ->
          Lwt.finalize begin fun () ->
            send_metrics_from_tags tags ;
            f level (pf ~msg ())
          end begin fun () ->
            over () ;
            Lwt.return_unit
          end
        end ;
        k () in
      Format.kasprintf k fmt
    in
    msgf m
  in
  { Logs.report = report }

let udp_reporter ?defs ?logs ?metrics () =
  make_reporter ?defs ?logs ?metrics (maybe_send send_udp logs)

(*---------------------------------------------------------------------------
   Copyright (c) 2019 Vincent Bernardoff

   Permission to use, copy, modify, and/or distribute this software for any
   purpose with or without fee is hereby granted, provided that the above
   copyright notice and this permission notice appear in all copies.

   THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
   WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
   MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
   ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
   WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
   ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
   OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
  ---------------------------------------------------------------------------*)
